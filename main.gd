extends Node

# Scene node objects
@onready var mnuCountry = $Align/mnuCountry/Button
@onready var txtProduct = $Align/txtProduct
@onready var txtOrganization = $Align/txtOrganization
@onready var numYear = $Align/boxDate/numYear
@onready var numMonth = $Align/boxDate/numMonth
@onready var numDay = $Align/boxDate/numDay

var fn_cert := "ssl.crt"
var fn_key  := "priv.key"

func _ready() -> void:
	# Add iso to menu
	var pop = mnuCountry.get_popup()
	for itm in iso3166.split(",\n", false): pop.add_item(itm)
	pop.id_pressed.connect(func(id): mnuCountry.text = pop.get_item_text(id))
	
	var time = Time.get_datetime_dict_from_system()
	numYear.min_value  = time["year"]
	numMonth.value = time["month"]
	numDay.value   = time["day"]

func _on_generate_pressed() -> void:
	if txtOrganization.text.strip_edges(true, true) == "" or\
		txtProduct.text.strip_edges(true, true) == "":
			OS.alert("Organization and Product cannot be left blank!")
			return
	
	var crypto := Crypto.new()
	var key    := crypto.generate_rsa(4096)
	var cert   := crypto.generate_self_signed_certificate(
		key, "CN=%s,O=%s,C=%s" %
		[txtProduct.text, txtOrganization.text, mnuCountry.text.left(2)],
		_get_date_before(), _get_date_after()
	)
	
	var dir  := Directory.new()
	var path := "./%s/%s/%s/" %\
		[txtOrganization.text, txtProduct.text, _get_date_before()]
	
	if not dir.dir_exists(path): dir.make_dir_recursive(path)
	
	cert.save(path + fn_cert)
	key.save(path + fn_key)
	
	OS.alert("Operation complete!", "DTLS Generator finished.")

func _get_date_before() -> String:
	return "%d%02d%02d000100" %\
		[numYear.value, numMonth.value, numDay.value]

func _get_date_after() -> String:
	return "%d%02d%02d000000" %\
		[numYear.value + 1, numMonth.value, numDay.value]

func _on_month_changed(value: float) -> void:
	match int(value):
		2:  numDay.max_value = 28
		4:  numDay.max_value = 30
		6:  numDay.max_value = 30
		9:  numDay.max_value = 30
		11: numDay.max_value = 30
		_:  numDay.max_value = 31

var iso3166 = "
	AD Andorra,
	AE United Arab Emirates,
	AF Afghanistan,
	AG Antigua and Barbuda,
	AI Anguilla,
	AL Albania,
	AM Armenia,
	AN Netherland Antilles,
	AO Angola,
	AQ Antarctica,
	AR Argentina,
	AS American Samoa,
	AT Austria,
	AU Australia,
	AW Aruba,
	AZ Azerbaijan,
	BA Bosnia- Herzegovina,
	BB Barbados,
	BD Bangladesh,
	BE Belgium,
	BF Burkina Faso,
	BG Bulgaria,
	BH Bahrain,
	BI Burundi,
	BJ Benin,
	BM Bermuda,
	BN Brunei Darussalam,
	BO Bolivia,
	BR Brazil,
	BS Bahamas,
	BT Bhutan,
	BV Bouvet Island,
	BW Botswana,
	BY Belarus,
	BZ Belize,
	CA Canada,
	CC Cocos (Keeling) Islands,
	CF Central African Republic,
	CG Congo,
	CH Switzerland,
	CI Ivory Coast,
	CK Cook Islands,
	CL Chile,
	CM Cameroon,
	CN China,
	CO Colombia,
	CR Costa Rica,
	CS Czechoslovakia (obsolete),
	CU Cuba,
	CV Cape Verde,
	CX Christmas Island,
	CY Cyprus,
	CZ Czech Republic,
	DE Germany,
	DJ Djibouti,
	DK Denmark,
	DM Dominica,
	DO Dominican Republic,
	DZ Algeria,
	EC Ecuador,
	EE Estonia,
	EG Egypt,
	EH Western Sahara,
	ER Eritrea,
	ES Spain,
	ET Ethiopia,
	FI Finland,
	FJ Fiji,
	FK Falkland Isl.( Malvinas),
	FM Micronesia,
	FO Faroe Islands,
	FR France,
	FX France (Europe only),
	GA Gabon,
	GB Great Britain (UK),
	GD Grenada,
	GE Georgia,
	GF Guyana (Fr.),
	GH Ghana,
	GI Gibraltar,
	GL Greenland,
	GM Gambia,
	GN Guinea,
	GP Guadeloupe (Fr.),
	GQ Equatorial Guinea,
	GR Greece,
	GS South Georgia & South Sandwich Islands,
	GT Guatemala,
	GU Guam (US),
	GW Guinea Bissau,
	GY Guyana,
	HK Hong Kong,
	HM Heard & McDonald Islands,
	HN Honduras,
	HR Croatia,
	HT Haiti,
	HU Hungary,
	ID Indonesia,
	IE Ireland,
	IL Israel,
	IN India,
	IO British Indian Ocean Terr.,
	IQ Iraq,
	IR Iran,
	IS Iceland,
	IT Italy,
	JM Jamaica,
	JO Jordan,
	JP Japan,
	KE Kenya,
	KG Kyrgyz Republic,
	KH Cambodia,
	KI Kiribati,
	KM Comoros,
	KN St. Kitts Nevis Anguilla,
	KP Korea (North),
	KR Korea (South),
	KW Kuwait,
	KY Cayman Islands,
	KZ Kazachstan,
	LA Laos,
	LB Lebanon,
	LC Saint Lucia,
	LI Liechtenstein,
	LK Sri Lanka,
	LR Liberia,
	LS Lesotho,
	LT Lithuania,
	LU Luxembourg,
	LV Latvia,
	LY Libya,
	MA Morocco,
	MC Monaco,
	MD Moldova,
	MG Madagascar,
	MH Marshall Islands,
	MK Macedonia (prev. Yug.),
	ML Mali,
	MM Myanmar,
	MN Mongolia,
	MO Macau,
	MP Northern Mariana Islands,
	MQ Martinique (Fr.),
	MR Mauritania,
	MS Montserrat,
	MT Malta,
	MU Mauritius,
	MV Maldives,
	MW Malawi,
	MX Mexico,
	MY Malaysia,
	MZ Mozambique,
	NA Namibia,
	NC New Caledonia Fr.,
	NE Niger,
	NF Norfolk Island,
	NG Nigeria,
	NI Nicaragua,
	NL Netherlands,
	NO Norway,
	NP Nepal,
	NR Nauru,
	NU Niue,
	NZ New Zealand,
	OM Oman,
	PA Panama,
	PE Peru,
	PF Polynesia (Fr.),
	PG Papua New Guinea,
	PH Philippines,
	PK Pakistan,
	PL Poland,
	PM St. Pierre & Miquelon,
	PN Pitcairn,
	PR Puerto Rico (US),
	PT Portugal,
	PW Palau,
	PY Paraguay,
	QA Qatar,
	RE Reunion (Fr.),
	RO Romania,
	RU Russian Federation,
	RW Rwanda,
	SA Saudi Arabia,
	SB Solomon Islands,
	SC Seychelles,
	SD Sudan,
	SE Sweden,
	SG Singapore,
	SH St. Helena,
	SI Slovenia,
	SJ Svalbard& Jan Mayen Islands,
	SK Slovakia (Slovak Republic),
	SL Sierra Leone,
	SM San Marino,
	SN Senegal,
	SO Somalia,
	SR Suriname,
	ST St. Tome and Principe,
	SU Soviet Union (obsolete),
	SV El Salvador,
	SY Syria,
	SZ Swaziland,
	TC Turks& Caicos Islands,
	TD Chad,
	TF French Southern Territory,
	TG Togo,
	TH Thailand,
	TJ Tadjikistan,
	TK Tokelau,
	TM Turkmenistan,
	TN Tunisia,
	TO Tonga,
	TP East Timor,
	TR Turkey,
	TT Trinidad Tobago,
	TV Tuvalu,
	TW Taiwan,
	TZ Tanzania,
	UA Ukraine,
	UG Uganda,
	UK United Kingdom,
	UM US Minor outlying Islands,
	US United States,
	UY Uruguay,
	UZ Uzbekistan,
	VA Vatican City State,
	VC St. Vincent& Grenadines,
	VE Venezuela,
	VG VirginIslands (GB),
	VI Virgin Islands (US),
	VN Vietnam,
	VU Vanuatu,
	WF Wallis & Futuna Islands,
	WS Samoa,
	YE Yemen,
	YT Mayotte,
	YU Yugoslavia (obsolete),
	ZA South Africa,
	ZM Zambia,
	ZR Zaire,
	ZW Zimbabwe,
"
